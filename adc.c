/*
 * File:   adc.c
 * Author: Urakami
 *
 * Created on August 29, 2017,  1:45 am
 */

#include "adc.h"

/**
 * adc_control_register
 * 
 * This function is used in adc_communication_brank(normal and shutdown).
 * Writes to 16-bit control register on the SCLK.
 * The pins where the ADC is connected are choose argument in
 * adc_communication_brank(normal and shutdown).
 * 
 * @pre SPI_CK_MASTER == 1
 * 
 * Note
 * -------
 * Data to be written to the AD7927 control register is provided on this input
 * and is clocked into the register on the falling edge of SCLK. 
 * Write the data before SCLK fall.
 */
static void adc_control_register(const unsigned int control_register){
    // CLK is high - it's set in select slave
    for(uint16_t mask = 0x8000 ; mask ; mask >>= 1){
        SPI_MOSI_MASTER = (control_register & mask ? 1 : 0); //Write ctlreg for ADC to read the bits on CLK falling edge.  
        SPI_CK_MASTER = 0;
        _delay(28);
        SPI_CK_MASTER = 1;
    }
}

/**
 * adcRead
 * 
 * This function is used in adc_communication_brank(normal and shutdown).
 * Reads to 16-bit conversion result on the SCLK. Return 16-bit data.
 * The data stream from the ADC consists of one leading zero, three address bits indicating 
 * which cannel the conversion result corresponds to followed by the 12bits of conversion data.
 * 
 * Note
 * -------
 * The bits are clocked out on the falling edge of the SCLK input.
 * Read the data after SCLK fall.
 */

 static uint16_t adcRead(void){
    uint16_t rx = 0;
    // CLK is high - it's set in select slave
    for(unsigned char loop = 16; loop; --loop){
        rx <<= 1;
        rx |= SPI_MISO_MASTER; // Read the bits on CLK falling edge.
        SPI_CK_MASTER = 0;
        _delay(28);
        SPI_CK_MASTER = 1;
    }
    return rx;
}

/**
 * adc_communication_normal
 * 
 * Writes control register and Reads conversion result.
 * Use select_slave, adc_control_register and adcRead functions for communication
 * with ADC.
 * 
 * Note
 * -------
 * First, define PORT and PIN, channel, range and outcoding in argument by the
 * structure adc_communication_information_t.
 * The structure is defined by header in TenkouADC.
 * 
 * Usage example:   Define in any order
 *  ------------------------------------------------------------------
 *  adc_communication_information_t <NAME>;
 *      <NAME>.port = &PORT<X>; 
 *      <NAME>.pin = 0 ~ 7;
 *      <NAME>.range_select = SINGLE_RANGE or DOUBLE_RANGE;
 *      <NAME>.outcoding_select = STRAIGHT_BINARY or TWOS_COMPLEMENT;
 *      <NAME>.channel_select = 0 ~ 7;
 *  ------------------------------------------------------------------
 *  <X>: A ~ E
 *  <NAME>: Freely name the structure
 *  
 * The detail:
 *  <NAME>.port and <NAME>.pin: Select the slave select pin which is connected on devise.
 * 
 *  <NAME>.channel_select: Select analog input channel.
 * 
 *  <NAME>.range_select: Select the input range for all input channels.
 *      SINGLE_RANGE: 0v to Reference voltage
 *      DOUBLE_RANGE: 0v to Reference voltage × 2
 * 
 *  <NAME>.outcoding_select: Select the type of output coding.
 *      STRAIGHT_BINARY: The output coding from the part is straight binary.
 *      TWOS_COMPLEMENT: The output coding for the part is twos complement.
 * 
 * 
 */
uint16_t  adc_communication_normal(adc_communication_information_t trans_data){ //adc communication system in normal mode
    uint16_t control_register = 0x8300 | (trans_data.channel_select << 10) | (trans_data.range_select << 5) | (trans_data.outcoding_select << 4);
    
    select_slave(trans_data.port,trans_data.pin,1); //Start condition for writing the data.
    adc_control_register(control_register); //Write the data control register to ADC.
    __delay_us(1);//tquiet
    
    select_slave(trans_data.port,trans_data.pin,1);   //Start condition for reading the data.
    uint16_t getdata = adcRead();   //Read the data conversion result.
    
    setPinHigh(trans_data.port,trans_data.pin); //Exit condition
    
    return getdata; 
}

/**
 * adc_communication_shutdown
 * 
 * ADC is in full shutdown mode with all circuitry on the ADC powering down.
 * 
 * Note
 * -------
 * If use after called adc_communication_normal, don't need define structure.
 * If use before call adc_communication_normal, need define structure like the above.
 */
void  adc_communication_shutdown(adc_communication_information_t trans_data){ //adc communication system in full shutdown mode
    uint16_t control_register = 0x8200 | (trans_data.channel_select << 10) | (trans_data.range_select << 5) | (trans_data.outcoding_select << 4);
    
    select_slave(trans_data.port,trans_data.pin,1);
    adc_control_register(control_register);
    __delay_us(1);//tquiet
    
    setPinHigh(trans_data.port,trans_data.pin);
}

/**
 * adc_conversion_voltage
 * 
 * Convert the binary result of the ADC conversion obtained by adc_read to an
 * actual voltage reading.
 * 
 * Attributes:
 * ------------
 * * data: binary conversion result read from the ADC,
 * * range: the voltage range in which ADC was used (1 or 2 Vref).
 */
float adc_conversion_voltage(uint16_t data,unsigned char range){
    uint16_t n_LSB = data & 0x0FFF;

    float Vref = (range ? REFERENCE_VOLTAGE : REFERENCE_VOLTAGE * 2);
    return (Vref/4096) * n_LSB;
}