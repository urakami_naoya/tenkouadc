/*
 * File:   main.c
 * Author: urakami
 *
 * Created on August 29, 2017,  1:45 am
 */

#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

#include "adc.h"
#include "usart.h"

int test_adc_communication(void){
    spi_pins_initialization();
    
    uint16_t data[8] = {0};
    float result[8] = {0};    //To save result of conversion_voltage
    uint8_t dat[2];
    uint8_t blank[2];
    blank[0] = 0x99;
    
    adc_communication_information_t trans_data;
        trans_data.port = &PORTB;
        trans_data.pin = 0;
        trans_data.range_select = DOUBLE_RANGE;
        trans_data.outcoding_select = STRAIGHT_BINARY; 
     
    for(unsigned char i = 0 ; i < 2 ; i++){
        trans_data.channel_select = i;
        data[i] = adc_communication_normal(trans_data);
        result[i] = adc_conversion_voltage(data[i],DOUBLE_RANGE);
       // __delay_us(1); //for debug mode
    }
        
    for(unsigned char j = 0 ; j < 2 ; j++){
        dat[1] = (uint8_t)data[j];
        dat[0] = (uint8_t)(data[j] >> 8);
//        usart_send_nBytes(blank,1);
//        usart_send_nBytes(dat,2);
    }
}

//test_duty_cycle(void) {
//    SPI_MOSI_MASTER_INIT = 0;
//    SPI_MISO_MASTER_INIT = 1;
//    SPI_CK_MASTER_INIT = 0;
//    TRISD0 = 0;
//    uint16_t reg = 0x5555;
//    for(;;) {
//        select_slave(&PORTD, 0, 1);
//        //adc_control_register(reg);
//        adcRead();
//        setPinHigh(&PORTD,0);
//        __delay_ms(10);
//    }
//}

void main(void) {
    //usart_initialization();
    int a = test_adc_communication();
    //int b = test_duty_cycle();
}
