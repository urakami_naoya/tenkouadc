/*
 * UART functions that work on an 8 MHz clock used in the PIC explorer boards
 * by default (our UART library works on 20 MHz). Only used here.
 */
#ifndef USART
#define	USART
#include <xc.h>
#include <stdio.h>

void usart_initialization(void);
void usart_send_nBytes(unsigned char* const data, size_t nBytes);

#endif