/* 
 * File:   adc.h
 * Author: urakaminaoya
 *
 * Created on August 29, 2017, 1:42 AM
 */

#ifndef ADC_H
#define	ADC_H

#define REFERENCE_VOLTAGE 2.5 //To use conversion_voltage function to check voltage. Don't need for ten_koh.
#define SINGLE_RANGE 1
#define DOUBLE_RANGE 0
#define STRAIGHT_BINARY 1
#define TWOS_COMPLEMENT 0

#include "spi_pins.h"

typedef struct adc_communication_information {  
    volatile unsigned char *port;
    unsigned char pin;
    unsigned char channel_select;
    unsigned char range_select;
    unsigned char outcoding_select;
} adc_communication_information_t;


// Declarations.
static void adc_control_register(const unsigned int control_register);
static uint16_t adcRead(void);

uint16_t adc_communication_normal(adc_communication_information_t trans_data);
void  adc_communication_shutdown(adc_communication_information_t trans_data);
float adc_conversion_voltage(uint16_t data,unsigned char range);
#endif // End header safegurad.