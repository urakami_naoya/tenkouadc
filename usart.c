#include "usart.h"

void usart_initialization(void){
    TRISC6 = 0;
    TRISC7 = 1;
    
    TXEN = 1;
    SYNC = 0;
    
    SPEN = 1; //RC7:RX RC6:TX
    CREN = 1;
    SPBRG = 12; //for 9600bps
}

void usart_send_nBytes(unsigned char* const data,size_t nBytes){
    for(unsigned int loop = 0 ; loop < nBytes ; loop++){
        while(!TRMT);
        TXREG = *(data + loop);
    }
}